<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
        // SOAL NOMOR 1
        //while 
        echo "<h3>Soal No 1 Looping I Love PHP menggunakan While</h3>"; //soal No. 1
        echo "<h5>LOPPING PERTAMA</h5>"; // Looping Pertama
        $i = 2;
        while($i <= 20) {
            echo "$i - I Love PHP <br>";
            $i+=2;
        }

        echo "<h5>LOOPING KEDUA</h5>"; // Looping Kedua
        $i = 20;
        while($i >= 2) {
            echo "$i - I Love PHP <br>";
            $i-=2;
        }

        // SOAL NOMOR 2
        echo "<h3>Soal No 2 Looping Array Modulo </h3>";
        $angka = [18, 45, 29, 61, 47, 34];
        echo "array angka : ";
        print_r($angka);
        echo "<br>";
        echo "Array sisa baginya adalah : ";
        foreach($angka as $value) {
            $rest[] = $value %= 5;
        }
        print_r($rest);
        echo "<br>";

        // SOAL NOMOR 3
        echo "<h3>Soal No 3 Looping Asociative Array </h3>";
        $produk = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg'],
        ];

        foreach($produk as $key => $value) {
            $item = array (
                'id' => $value[0],
                'name' => $value[1],
                'price' => $value[2],
                'description' => $value[3],
                'source' => $value[4]
            );
            print_r($item);
            echo "<br>";
        }

        // SOAL NOMOR 4
        echo "<h3>Soal No 4 Asterix</h3>";
        for($i=1; $i<=5; $i++) {
            for($j=1; $j<=$i; $j++){
                echo "*";
            }
            echo "<br>";
        }
    ?>
</body>
</html>